import QtQuick 2.0
import "../../Components/Circle"
import "../../Components/Utils"

CircleFramed{
    id: item

    property bool smoothing: false

    Behavior on x{
        enabled: item.smoothing
        SmoothedAnimation{}
    }

    Behavior on y{
        enabled: item.smoothing
        SmoothedAnimation{}
    }

    Behavior on scale{
        enabled: item.smoothing
        SmoothedAnimation{}
    }

    transform: Translate { x: -item.width/2; y: -item.height/2 }

}
