import QtQuick 2.0
import AstraMuseum 1.0
import "../../Components/TitleText"
import "../../Components/Utils/Constants.js" as Constants

Item {
    id: bodBox

    property ListModel listModel: currentListModel
    property var title: currentTitle
    property string locale : currentLocale

    Box{
        id: box
        anchors.fill: parent
        listModel: bodBox.listModel
        title: bodBox.title
    }

    Rectangle{
        width: 100
        height: 100
        radius: width/2
        antialiasing: true
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.margins: 50
        opacity: 0.5

        color: "green"

        visible: container.debugMode && !loadingItem.loading

        Text{
            text : "Back"
            anchors.centerIn: parent
        }

        MouseArea{
            anchors.fill: parent
            onClicked: {
                listModel.clear()
                loader.source = Qt.resolvedUrl("../../Settings.qml")
            }
        }
    }

    TitleText{
        id: title
        anchors.top: parent.top
        anchors.topMargin: 50
        fontColor: "white"
        fontPixelSize: 40
        text: bodBox.title[bodBox.locale]
        visible: !loadingItem.loading
        anchors.horizontalCenter: parent.horizontalCenter
    }

    TitleText{
        id: yearText
        anchors.top: title.bottom
        //anchors.topMargin: 5
        fontColor: "white"
        fontPixelSize: 36
        text: box.currentYear
        visible: !loadingItem.loading
        anchors.horizontalCenter: parent.horizontalCenter
    }

    Timer{
        id: delay
        interval: 250
        onTriggered: {
            Content.init("C:/App/Data", "BOD")
            Content.list()
        }
    }

    Component.onCompleted: {
        loadingItem.show();
        delay.start()
    }
}
