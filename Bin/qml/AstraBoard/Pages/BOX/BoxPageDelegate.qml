import QtQuick 2.0
import "../../Components/Circle"
import "../../Components/TitleText"
import "../../Components/TextBoxCanvas"

import AstraMuseum 1.0

Item {

    id: pageDelegate
    property var year : currentYear
    property string folderName : currentFolderName
    property ListModel people : currentPeople
    property var mainPerson: currentMainPerson
    property string locale: currentLocale

    property bool isCurrentIndex: boxMenu.selectedIndex == index
    property bool isCurrentIndexPlusNeighbor : (isCurrentIndex || (boxMenu.selectedIndex + 1 == currentIndex) || (boxMenu.selectedIndex - 1 == currentIndex))
    property int currentIndex: index

    property int count: currentCount
    property int peopleCount: currentPeopleCount
    property bool groupPicture: currentGroupPicture

    property double itemSize : 438
    property double cellSize : pageDelegate.itemSize * ( pageDelegate.peopleCount < 4 ? 0.65 : 0.5)
    property int magicNumber: pageDelegate.peopleCount >= 12 ? 5 : 4
    property int gridWidth : pageDelegate.peopleCount < 4 ? pageDelegate.peopleCount * pageDelegate.cellSize : pageDelegate.magicNumber * pageDelegate.cellSize
    property int gridHeight : Math.ceil(pageDelegate.peopleCount/pageDelegate.magicNumber) * pageDelegate.cellSize
    property int leftRectWidth: pageDelegate.peopleCount > 0 ? parent.width/3 : (parent.width/2 + pageDelegate.itemSize/2)

    property double mainPersonCircleScale : 0.8 //pageDelegate.groupPicture ? 1.0 : 0.8
    property double peopleCircleScale : pageDelegate.peopleCount < 4 ? 0.6 : 0.5

    property bool busy : false
    property int selectedIndex : -1
    property string selectedName : ""
    property bool selectedItemIsLeft : true

    /*onIsCurrentIndexPlusNeighborChanged: {
        delayLoad.start()
    }

    Timer{
        id: delayLoad
        interval: 500
        onTriggered: {
            loaderMainPerson.sourceComponent = pageDelegate.isCurrentIndexPlusNeighbor || boxMenu.isOpen ? mainCircleComponent : undefined
            //loaderPeople.sourceComponent = pageDelegate.isCurrentIndexPlusNeighbor || boxMenu.isOpen ? circleComponent : undefined
        }
    }*/

    Row{
        id: pageDelegateRow
        anchors.fill: parent

        Item{
            id: pageDelegateLeftRect
            height: parent.height
            width: pageDelegate.leftRectWidth

            Item{
                id: pageDelegateMainPerson
                width: pageDelegate.itemSize
                height: pageDelegateMainPerson.width
                anchors.verticalCenter: parent.verticalCenter
                anchors.right: parent.right

                property double circleScale: pageDelegate.mainPersonCircleScale
                property url image : pageDelegate.mainPerson.image
                property string nameTag : pageDelegate.mainPerson.nameTag

                Loader{
                    id: loaderMainPerson
                    sourceComponent: mainCircleComponent
                    anchors.fill: parent
                    asynchronous: true
                }

                Component{
                    id: mainCircleComponent
                Item{
                    anchors.fill: parent

                CircleFramed{
                    id: pageDelegateMainPersonCircle
                    scale: pageDelegateMainPerson.circleScale
                    source: pageDelegateMainPerson.image
                    text: pageDelegateMainPerson.nameTag
                    anchors.centerIn: parent
                    visible: !pageDelegate.groupPicture

                    onClicked: {
                        if(pageDelegate.selectedName.length == 0){
                            pageDelegate.selectedItemIsLeft = true
                            textBox.isLeft = true
                            pageDelegate.selectedName = pageDelegate.mainPerson.name
                            pageDelegate.action(-1, pageDelegateMainPerson);
                            localeSwitch.hide()
                        }
                    }
                }

                CircleGroupPicture{
                    source: pageDelegateMainPerson.image
                    visible: pageDelegate.groupPicture
                    anchors.centerIn: parent

                    onClicked: {

                    }
                }
                }
                }
            }
        }

        Item{
            id: pageDelegateRightRect
            height: parent.height
            width: parent.width - pageDelegateLeftRect.width
            visible: pageDelegate.peopleCount > 0

            Item{
                id: pageDelegatePeople
                anchors.fill: parent

                GridView{
                    id: pageDelegatePeopleGrid
                    width: pageDelegate.gridWidth
                    height: pageDelegate.gridHeight
                    anchors.centerIn: parent
                    anchors.horizontalCenterOffset: pageDelegate.magicNumber == 5 ? -50 : 0
                    cellWidth: pageDelegate.cellSize
                    cellHeight: pageDelegate.cellSize
                    interactive: false

                    model: pageDelegate.people
                    // verticalLayoutDirection: GridView.BottomToTop

                    delegate: Item{

                        id: pageDelegatePeopleGridItem

                        width: pageDelegate.cellSize
                        height: pageDelegatePeopleGridItem.width
                        opacity: model.visual.opacity

                        property double circleScale: pageDelegate.peopleCircleScale
                        property url image : model.image
                        property string nameTag : model.nameTag
                        property bool selected : model.selected

                        function animateSelectedItem(){
                            if(pageDelegate.selectedName.length == 0){
                                pageDelegate.selectedItemIsLeft = model.isLeft
                                textBox.isLeft = model.isLeft
                                pageDelegate.selectedName = model.name
                                pageDelegate.action(model.index, pageDelegatePeopleGridItem);
                                localeSwitch.hide()
                            }
                        }

                        onSelectedChanged: {
                            if(pageDelegatePeopleGridItem.selected) animateSelectedItem()
                        }

                        Loader{
                            id: loaderPeople
                            anchors.fill: parent
                            sourceComponent: circleComponent
                            asynchronous: true
                        }

                        Component{
                            id: circleComponent
                            CircleFramed{
                                id: circle
                                scale: pageDelegatePeopleGridItem.circleScale
                                source: pageDelegatePeopleGridItem.image
                                text: pageDelegatePeopleGridItem.nameTag
                                anchors.centerIn: parent

                                onClicked: {
                                    animateSelectedItem()
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    // state and transition
    state: "normal"
    states: [
        State{
            name: "normal"
        },
        State{
            name: "detached"
        }
    ]

    transitions: [
        Transition {
            from: "normal"
            to: "detached"
            SequentialAnimation{
                PropertyAction{ target: pageDelegate; property: "busy"; value: true}
                PropertyAction{ target: pageDelegateSpareItem; property: "prevX"; value: pageDelegateSpareItem.x}
                PropertyAction{ target: pageDelegateSpareItem; property: "prevY"; value: pageDelegateSpareItem.y}
                ParallelAnimation{
                    NumberAnimation { target: pageDelegatePeopleGrid; property: "scale"; duration: 500; easing.type: Easing.Linear; to: 0.5 }
                    NumberAnimation { target: pageDelegateMainPerson; property: "scale"; duration: 500; easing.type: Easing.Linear; to: pageDelegate.selectedIndex > -1 ? 0.5 : 1.0 }
                    NumberAnimation { target: pageDelegateSpareItem; property: "scale"; duration: 500; easing.type: Easing.Linear; to: pageDelegate.selectedIndex > -1 ? (pageDelegate.people.count  < 4 ? 1.6 : 1.93) : 1.20}
                    SmoothedAnimation { target: pageDelegateSpareItem; property: "x"; duration: 500; to: (pageDelegate.width/2 - pageDelegateSpareItem.width/2) + (pageDelegate.selectedItemIsLeft ? -285 : 285) }
                    SmoothedAnimation { target: pageDelegateSpareItem; property: "y"; duration: 500; to: pageDelegate.height/2 - pageDelegateSpareItem.height/2 }
                //}

                //ParallelAnimation{
                    NumberAnimation { target: pageDelegatePeopleGrid; property: "opacity"; duration: 500; easing.type: Easing.OutSine; to: 0.0 }
                    NumberAnimation { target: pageDelegateMainPerson; property: "opacity"; duration: 500; easing.type: Easing.OutSine; to: 0.0}
                }

                PropertyAction{ target: pageDelegateSpareItem; property: "stripsVisible"; value: true}
                PauseAnimation { duration: 400 }
                PropertyAction { target: textBox; property: "visible"; value: true }
                PropertyAction{ target: pageDelegate; property: "busy"; value: false}
            }

        },
        Transition {
            from: "detached"
            to: "normal"

            SequentialAnimation{

                PropertyAction{ target: pageDelegateSpareItem; property: "stripsVisible"; value: false}
                PauseAnimation { duration: 350 }

                PropertyAction{ target: pageDelegate; property: "busy"; value: true}
                ParallelAnimation{
                    NumberAnimation { target: pageDelegatePeopleGrid; property: "opacity"; duration: 500; easing.type: Easing.OutSine; to: 0.1 }
                    NumberAnimation { target: pageDelegateMainPerson; property: "opacity"; duration: 500; easing.type: Easing.OutSine; to: pageDelegate.selectedIndex > -1 ? 0.1 : 0.0 }
                    NumberAnimation { target: pageDelegatePeopleGrid; property: "scale"; duration: 500; easing.type: Easing.Linear; to: 1.0 }
                    NumberAnimation { target: pageDelegateMainPerson; property: "scale"; duration: 500; easing.type: Easing.Linear; to:  1.0  }
                    NumberAnimation { target: pageDelegateSpareItem; property: "scale"; duration: 500; easing.type: Easing.Linear; to: 1.0 }
                    SmoothedAnimation { target: pageDelegateSpareItem; property: "x"; duration: 500;  to: pageDelegateSpareItem.prevX }
                    SmoothedAnimation { target: pageDelegateSpareItem; property: "y"; duration: 500; to: pageDelegateSpareItem.prevY }
                    NumberAnimation { target: pageDelegatePeopleGrid; property: "opacity"; duration: 500; easing.type: Easing.OutSine; to: 1.0 }
                    NumberAnimation { target: pageDelegateMainPerson; property: "opacity"; duration: 500; easing.type: Easing.OutSine; to: pageDelegate.selectedIndex > -1 ? 1.0 : 0.0 }
                }
                ScriptAction{ script: pageDelegate.reset()}
            }
        }
    ]

    function selectMainPerson(){
        pageDelegate.selectedItemIsLeft = true
        textBox.isLeft = true
        pageDelegate.selectedName = pageDelegate.mainPerson.name
        pageDelegate.action(-1, pageDelegateMainPerson);
        localeSwitch.hide()

    }


    function action(index, item){
        //if(pageDelegate.busy || pageDelegate.state == "detached") return;
        nav.visible = false

        pageDelegate.selectedIndex = index
        for(var i = 0; i  < pageDelegate.people.count; i++){
            if(i == index){
                pageDelegate.people.get(i).visual = {opacity: 0.0, scale : 1.0}
                pageDelegateSpareItem.source = pageDelegate.people.get(i).image
            }else{
                pageDelegate.people.get(i).visual = {opacity: 1.0, scale : 1.0}
            }
        }

        if(pageDelegate.selectedIndex == -1){
            pageDelegateSpareItem.source = item.image
            item.opacity = 0.0
        }

        pageDelegateSpareItem.width = item.width
        pageDelegateSpareItem.height = item.height
        pageDelegateSpareItem.circleScale = item.circleScale
        pageDelegateSpareItem.nameTag = item.nameTag
        pageDelegateSpareItem.x = item.mapToItem(pageDelegate).x
        pageDelegateSpareItem.y = item.mapToItem(pageDelegate).y
        pageDelegateSpareItem.visible = true

        pageDelegate.state = "detached"
        boxMenu.hide()

        Content.readText(pageDelegate.folderName, pageDelegate.selectedIndex, pageDelegate.locale, pageDelegate.selectedName)
    }

    function reset(){
        if(pageDelegate.selectedIndex > -1)
            pageDelegate.people.get(pageDelegate.selectedIndex).visual = {opacity: 1.0, scale : 1.0}
        else{
            pageDelegateMainPerson.opacity = 1.0
        }

        pageDelegateSpareItem.visible = false
        pageDelegate.selectedIndex = -1
        pageDelegate.busy = false

        textBox.visible = false
        nav.visible = true

    }

    /*Connections{
        target: boxListView
        onMovingChanged: {
            if(boxListView.moving && index == boxMenu.selectedIndex ){
                console.log(pageDelegate.mapToItem(container, 0, 0).x)
            }
        }
    }*/

    Loader{
        id: connectionLoader
        sourceComponent: boxMenu.selectedIndex == index ? connectionItem : undefined
        asynchronous: true
        onSourceComponentChanged: {
            debugItem.setText(pageDelegate.folderName)
        }
    }

    Component{
        id: connectionItem
    Item{

    Connections{
        target: Content
        onReadTextResponse: {
            //debugItem.setText(JSON.stringify(jsonData))
            if(jsonData){
                textBox.titleText = jsonData.title ? jsonData.title : ""
                textBox.subtitleText = jsonData.subtitle ? jsonData.subtitle : ""
                textBox.contentText = jsonData.text ? jsonData.text : ""
            }
        }
    }

    Connections{
        target: localeSwitch
        onIsIDChanged : {
            if(textBox.isOpen){
                textBox.swicthLocale()
            }
        }
    }

    Connections{
        target: textBox
        onClosed: {
            pageDelegate.state = "normal"
            delayLocaleSwitchShow.start()
        }
        onClosing: {
            localeSwitch.hide()
        }
        onFinished: {
            if(textBox.isOpen){
                localeSwitch.setCenter()
                localeSwitch.show()
            }
        }

        onLoadLocale: {

            Content.readText(pageDelegate.folderName, pageDelegate.selectedIndex, pageDelegate.locale, pageDelegate.selectedName);
        }

    }

    Timer{
        id: delayLocaleSwitchShow
        interval: 200
        onTriggered: {
            localeSwitch.setRight()
            localeSwitch.show()
            boxMenu.show()
            pageDelegate.selectedName = ""
        }
    }

    Connections{
        target: simulateObject
        onSimulateSelectPersonInGrid: {

            if(selectedPersonIndex == -1){
                pageDelegate.selectMainPerson()
            }else{
                for(var i = 0; i < pageDelegate.people.count; i++){
                    if(i == selectedPersonIndex){
                        pageDelegate.people.get(i).selected = true
                    }else{
                        pageDelegate.people.get(i).selected = false
                    }
                }
            }
            debugItem.setText('selectedPersonIndex: ' + selectedPersonIndex)
        }

        onGetPeopleCount:{
            simulateObject.peopleCount(pageDelegate.people.count)
        }
    }

    }
    }


}
