import QtQuick 2.0

Item {
    id: circleFramed
    width: 438
    height: 438

    signal clicked()
    signal finished()

    property alias source: box.source
    property alias text: box.text
    property bool stripsVisible : false
    property bool animate : false

    onStripsVisibleChanged: {
        strips.opacity = 1.0
        circleFramed.animate = true
        delay.start()
    }

    Timer{
        id: delay
        interval: 500
        onTriggered: {
            //strips.visible = circleFramed.stripsVisible
            strips.opacity = circleFramed.stripsVisible ? 1.0 : 0.0
            circleFramed.animate = false
        }
    }

    Item{
        id: strips
        width: 438
        height: 438

        opacity: 0.0
        visible: opacity > 0.0

        Behavior on opacity{
            SmoothedAnimation{}
        }

        Image{
            id: strip0
            source: "../../Assets/Images/image-circle-strip0.png"
            anchors.centerIn: parent

            SequentialAnimation on rotation{
                running: circleFramed.animate
                NumberAnimation { from: 0; to: 360; duration: 500; easing.type: Easing.InOutSine  }
            }
        }

        Image{
            id: strip1
            source: "../../Assets/Images/image-circle-strip1.png"
            anchors.centerIn: parent

            SequentialAnimation on rotation{
                running: circleFramed.animate
                NumberAnimation { from: 0; to: 360; duration: 500; easing.type: Easing.InOutSine  }
            }
        }

        Image{
            id: strip2
            source: "../../Assets/Images/image-circle-strip2.png"
            anchors.centerIn: parent

            SequentialAnimation on rotation{
                running: circleFramed.animate
                NumberAnimation { from: 0; to: 360; duration: 500; easing.type: Easing.InOutSine  }
            }
        }

        Image{
            id: strip3
            source: "../../Assets/Images/image-circle-strip3.png"
            anchors.centerIn: parent

            SequentialAnimation on rotation{
                running: circleFramed.animate
                NumberAnimation { from: 0; to: 360; duration: 500; easing.type: Easing.InOutSine  }
            }
        }
    }

    CirclePerson{
        id: box
        anchors.centerIn: parent
        onClicked: circleFramed.clicked()

    }

    Rectangle{
        color: "transparent"
        border.color: "white"
        border.width: 1
        width: box.width
        height: box.height
        radius: width/2
        anchors.centerIn: box
        antialiasing: true
        opacity: 0.5
    }


}
