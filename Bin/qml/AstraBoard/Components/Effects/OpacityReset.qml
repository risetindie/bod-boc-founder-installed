import QtQuick 1.1
import Qt.labs.shaders 1.0

OpacityEffect {

    image: ShaderEffectSource {
        sourceItem : Item {}
        live: true
        hideSource: true
    }

    mask: ShaderEffectSource {
        sourceItem : Item {}
        live: true
        hideSource: true
    }
}
