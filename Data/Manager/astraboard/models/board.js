var boards = [
  { name : 'Founder'},
  { name : 'BOD'},
  { name : 'BOC'},
  { name : 'Test'}
]

exports.list = function(cb){
  cb(null, boards)
}

exports.meta = function(board, cb){
  var rootPath = __dirname + '/../../../Contents/' + board + '/People';
  var str_id_ID = fs.readFileSync(rootPath + '/id_ID.json')
  var str_en_US = fs.readFileSync(rootPath + '/en_US.json')

  var obj_id_ID = JSON.parse(str_id_ID)
  var obj_en_US = JSON.parse(str_en_US)

  cb(null, { 
    en_US : { title : obj_en_US.title, menuTitle : obj_en_US.menuTitle}, 
    id_ID : { title : obj_id_ID.title, menuTitle : obj_id_ID.menuTitle}
  })
}
