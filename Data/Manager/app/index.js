function ViewModel(){
  var self = this
  self.currentIsID = ko.observable(false)
  self.currentLocale = ko.observable('en_US')
  self.currentDataType = ko.observable("")
  self.currentPeriods = ko.observableArray([])
  self.currentSelectedPeriod = ko.observable("")
  self.currentModalDataType = ko.observable("")

  self.currentPeople = ko.observable([])

  self.currentEditMode = ko.observable(false)
  self.currentPersonAttr = ko.observable({})

  self.currentTempImagePath = ko.observable("")
  self.sequenceIsChanged = ko.observable(false)

  self.currentTitle = ko.observable({})
  self.currentMenuTitle = ko.observable({})
}

function pad(num, size) { var s = "000000000" + num; return s.substr(s.length-size);}

var viewModel = new ViewModel()
ko.applyBindings(viewModel)

function init(){
  $('#board-test').click()
}

function people(board, year){

  dragClear()
  viewModel.currentPeople([])

  console.log(board)
  console.log(year)

  $.get('/people', { board : board, year : year}, function(data){
    var d = data.data

    console.log(d)
  
    var peeps = []
    for(var i = 0; i < d.people.length; i++){
      var person = d.people[i]
      var imgUrl = '/Contents/' + board + '/People/' + d.folderName + '/' + person.position + '/' + person.image
      var obj = { 
        image : imgUrl,
        name : person.name,
        index : person.index,
        position : person.position
      }
      peeps.push(obj)
    }
    viewModel.currentPeople(peeps)

    $('.person-item-editable').unbind('click')
    $('.person-item-editable').click(function(){
      viewModel.currentEditMode(false)
      updatePerson(this, 'person', 'People')
      return false
    })
    dragInit()

  })
  .error(function(){

  })
}

function savePeriod(en){

  $.post('/period/add',{
    board : viewModel.currentDataType(),
    year : en
  },
  function(data){
    closeInputModal()
    loadPeriod()
  }).error(function(err){
    showAlertError(err.responseText)
  })
}

function removePeriod(en){

  $.post('/period/remove', 
  { 
    board : viewModel.currentDataType(),
    year : en
  },
  function(data){
    loadPeriod()
    closeInputModal()
  }).error(function(err){
    showAlertError(err.responseText)
  })
}

function updatePeriodID(){
  $('#period-id').val('')
  var a = $('#period-en').val()
    if(a.indexOf(' ') > -1){
      var c = a.substring(a.indexOf(' ') + 1, a.length)
      if(parseInt(c) > 1956){
        var b = moment(a, 'MMMM YYYY')
        b.lang('id')
        $('#period-id').val(b.format('MMMM YYYY'))  
      }
    }else{
      if(parseInt(a) > 1956) $('#period-id').val(a)
  }
}

function closeInputModal(){
  $('#input-modal').foundation('reveal', 'close')
}

function showInputModal(el, type, prefix){
  $('#input-modal').foundation('reveal', 'open')
  viewModel.currentModalDataType(type)
}

function updatePerson(el, type, prefix){
  var board = viewModel.currentDataType()
  var year = viewModel.currentSelectedPeriod()
  var name = $(el).children('.person-item-name').val()
  var position = $(el).children('.person-item-position').val()
  var index = $(el).children('.person-item-index').val()
  var image = $(el).children('img').attr('src')

  loadPerson(board, year, name, function(err, data){
    $('#person-image').attr('src', image)
    viewModel.currentPersonAttr({
      name : name,
      position : position,
      index : index,
      image : image,
      board : board,
      year : year,
      data : data.data
    })

    showInputModal(el, type, prefix)
    updatePersonView()
  })
}

function loadPerson(board, year, name, cb){
  $.get('/person/show', { board : board, year : year, name : name}, function(data){
    cb(null, data)
  }).error(function(){ cb({error : true})})
}

function addPeriodItemClicked(el, type, prefix){
  $('#period-id').val('')
  showInputModal(el, type, prefix)
}

function updatePeriodItemClicked(el, type, prefix){
  showInputModal(el, type, prefix)
}

function loadPeriod(){
  $.get('/board/periods?board=' + 
    viewModel.currentDataType(), function(res){

    var data = []
    var resData = res.data.data

    viewModel.currentTitle(res.data.meta.title)
    viewModel.currentMenuTitle(res.data.meta.menuTitle)

    for(var i = 0; i < resData.length; i++){
      var obj = { 
        year : resData[i].en_US,
        period: resData[i].en_US.indexOf(' ') > -1 ?  resData[i].en_US.split(' ').join('<br />') : resData[i].en_US,
        folderName: resData[i].folderName,
        lastFolder : false
      }
      data.push(obj)
    }
    data.push({ year : 'add', period: '<b>Add</b>', lastFolder : true})
    viewModel.currentPeriods(data)

    $('.period-item').unbind('click')
    $('.period-item').click(function(){
      return periodItemClicked(this)
    })

    $('.period-item')[0].click()
    return false
  })
}

function closeAlert(){
  $('#input-modal-alert').removeClass('alert')
  $('#input-modal-alert').hide()
}

function showAlertError(msg){
  $('#input-modal-alert').addClass('alert')
  $('#input-modal-alert').html(msg)
  $('#input-modal-alert').show() 

  setTimeout(function(){$('#input-modal-alert').hide() }, 1000)
}

function showAlertInfo(msg){
  $('#input-modal-alert').removeClass('alert')
  $('#input-modal-alert').html(msg)
  $('#input-modal-alert').show() 
}

function periodItemClicked(el){

  $('#input-modal-alert').hide()

  var period = $(el).data().period

  if(period !='add'){

    if(viewModel.currentSelectedPeriod() != period){
      $('.period-item').parent().removeClass('scrollable-item-selected')
      $('.period-item').children('div').removeClass('scrollable-item-text-selected')
      $(el).parent().addClass('scrollable-item-selected')
      $(el).children('div').addClass('scrollable-item-text-selected')
      viewModel.currentSelectedPeriod(period)

      people(viewModel.currentDataType(), period)

    }else{
      updatePeriodItemClicked(el, 'period-update', 'Period' )
      $('#period-en').val(period)
      updatePeriodID()

    }
    
  }else{
    $('#period-en').val('')
    addPeriodItemClicked(el, 'period-add', 'Period')
  }

  return false
}

$('.board').click(function(){

  viewModel.currentDataType($(this).data().type)
  
  $('.board').parent().removeClass('active')
  $(this).parent().addClass('active')

  loadPeriod()
  return false

})

$('#button-period-delete').click(function(){
  removePeriod($("#period-en").val())
})

$('#button-period-add').click(function(){
  var id = $("#period-id").val() 
  var en = $("#period-en").val()
  if(id && en){
    savePeriod(en)
  }else{
    showAlertError('Invalid period')
  }
})

$('#period-en').keyup(function(){
    updatePeriodID()
})

$('#person-edit-save').click(function(){
  
  if(!viewModel.currentEditMode()){
    viewModel.currentEditMode(true)
    updatePersonEdit()
  }
  else{
    //viewModel.currentEditMode(false)
    var title = $('#person-edit-title').val()
    var subtitle = $('#person-edit-subtitle').val()
    var shortname = $('#person-edit-shortname').val()
    var text = $('#person-edit-text').val()

  
    $.post('/person/edit', {
      board : viewModel.currentDataType(),
      year : viewModel.currentSelectedPeriod(),
      lang : viewModel.currentLocale(),
      title : title,
      subtitle : subtitle,
      shortname : shortname,
      text : text,
      name : viewModel.currentPersonAttr().name
    }, function(data){

      if(viewModel.currentTempImagePath() != ''){

        $.post('/image/save', {
          path : viewModel.currentTempImagePath(),
          name : viewModel.currentPersonAttr().name,
          board : viewModel.currentDataType(),
          year : viewModel.currentSelectedPeriod() 
        }, function(data){
          
          loadPerson(viewModel.currentDataType(), viewModel.currentSelectedPeriod(), viewModel.currentPersonAttr().name, function(err, res){
            var temp = viewModel.currentPersonAttr()
            temp.data = res.data
            viewModel.currentPersonAttr(temp)
            viewModel.currentEditMode(false)
            updatePersonView()

            people(viewModel.currentDataType(), viewModel.currentSelectedPeriod())
          })

        }).error(function(err){
          console.log("Error saving image")
        })

        viewModel.currentTempImagePath('')

      }else{

        loadPerson(viewModel.currentDataType(), viewModel.currentSelectedPeriod(), viewModel.currentPersonAttr().name, function(err, res){
          var temp = viewModel.currentPersonAttr()
          temp.data = res.data
          viewModel.currentPersonAttr(temp)
          viewModel.currentEditMode(false)
          updatePersonView()
        })
      }
    }).error(function(){
      console.log('error')
    })

  }
})

$('#add-person').click(function(){
  $('#person-add-name').val("")
  $('.primary').addClass('secondary')
  showInputModal(this, 'person-add', 'Person')
})

$('.lang').click(function(){
  $('.lang').addClass('secondary')
  $(this).removeClass('secondary')

  if(viewModel.currentLocale() == 'en_US')
    viewModel.currentLocale('id_ID')
  else
    viewModel.currentLocale('en_US')

  if(viewModel.currentEditMode()){
    updatePersonEdit()
  }else{
    updatePersonView()
  }


})

function updatePersonView(){

  var currentLocale = viewModel.currentLocale()
  var person = viewModel.currentPersonAttr()
  var text = person.data.text[currentLocale]
  var shortName = person.data.shortName

  $('#person-view-title').text(text.title)
  $('#person-view-subtitle').text(text.subtitle)
  $('#person-view-shortname').text(shortName ? shortName : 'empty')
  $('#person-view-text').html(text.text)

}

function updatePersonEdit(){
  var currentLocale = viewModel.currentLocale()
  var person = viewModel.currentPersonAttr()
  var text = person.data.text[currentLocale]
  var shortName = person.data.shortName

  $('#person-edit-title').val(text.title)
  $('#person-edit-subtitle').val(text.subtitle)
  $('#person-edit-shortname').val(shortName)
  $('#person-edit-text').val(text.text)
}


$('#person-delete-cancel').click(function(){

  if(viewModel.currentEditMode()){
    updatePersonView()
    viewModel.currentEditMode(false)
  }else{
    var currentPerson = viewModel.currentPersonAttr()

    $.post('/person/remove', {
      board : currentPerson.board,
      year : currentPerson.year,
      position : currentPerson.position,
      index : currentPerson.index,
      name : currentPerson.name,
      image : currentPerson.image
    }, function(data){
      people(viewModel.currentDataType(), viewModel.currentSelectedPeriod())
      closeInputModal()
    })
    .error(function(){

    })  
  }
  return false
})

$('#person-image-handle').click(function(){
  if(viewModel.currentEditMode()) $('#fileupload').trigger('click')
})

$('#person-save-add').click(function(){
  if($('#person-add-name').val() != ''){
    $.post('/person/add', {
      board : viewModel.currentDataType(),
      year : viewModel.currentSelectedPeriod(),
      name : $('#person-add-name').val(),
      primary : ($('.primary').attr('class').indexOf('secondary') == -1)
    }, function(data){
      people(viewModel.currentDataType(), viewModel.currentSelectedPeriod())
      closeInputModal()
    })
    .error(function(err){
      showAlertError(err.responseText)
    })
  }else{
    showAlertError('Name is required')
  }
})

$('.title-edit').click(function(){
  showInputModal(this, 'title-edit', '')
})

$('.menu-title-edit').click(function(){
  showInputModal(this, 'menu-title-edit', '')
})

$('#button-title-edit').click(function(){
  
  var en = $('#title-en').val()
  var id = $('#title-id').val()

  if(en != '' && id != ''){

    $.post('/title/edit', {
      board : viewModel.currentDataType(),
      en : en,
      id : id
    }, function(data){
      closeInputModal()
      loadPeriod()
    }).error(function(err){
      showAlertError(err.responseText)
    })

  }else{
    showAlertError('Both fields are required')
  }

  
})

$('#button-menu-title-edit').click(function(){

  var en = $('#menu-title-en').val()
  var id = $('#menu-title-id').val()

  if(en != '' && id != ''){

    $.post('/menutitle/edit', {
      board : viewModel.currentDataType(),
      en : en,
      id : id
    }, function(data){
      closeInputModal()
      loadPeriod()
    }).error(function(err){
      showAlertError(err.responseText)
    })

  }else{
    showAlertError('Both fields are required')
  }
})


$('.primary').click(function(){
  if($('.primary').attr('class').indexOf('secondary') > -1) $('.primary').removeClass('secondary')
  else $('.primary').addClass('secondary')
})

$(function () {
    $('#fileupload').fileupload({
        dataType: 'json',
        error : function(){
          console.log('error')
        },
        done: function (e, data) {
          viewModel.currentTempImagePath('/temp' + data.result.path)
          $('#person-image').attr('src', viewModel.currentTempImagePath())
        }
    });
});


init()



/*function ViewModel(){
  var self = this

  self.currentIsID = ko.observable(false)
  self.currentLocale = ko.observable('en_US')
  self.currentDataType = ko.observable("")
  self.currentPeriods = ko.observableArray([])
  self.currentPeriodsLength = ko.observable(0)
  self.currentMetaData = ko.observable({})

  self.currentPeople = ko.observableArray([])

  // selected person for editing
  self.currentSelectedPerson = ko.observable({})
  self.currentSelectedPersonIndex = ko.observable(-1)
  self.currentSelectedPersonEditMode = ko.observable(false)
  self.currentSelectedPersonImage = ko.observable("")

  // selected period for editing
  self.currentSelectedPeriod = ko.observable({})
  self.currentSelectedPeriodIndex = ko.observable(-1)
  self.currentSelectedFolderName = ko.observable("")

  // selected title for editing
  self.currentSelectedTitleID = ko.observable("")
  self.currentSelectedTitleEN = ko.observable("")
  self.currentSelectedMenuTitleID = ko.observable("")
  self.currentSelectedMenuTitleEN = ko.observable("")

  self.currentPositionsID = ko.observableArray([])
  self.currentPositionsEN = ko.observableArray([])
  self.currentPositionsStrID = ko.observable("")
  self.currentPositionsStrEN = ko.observable("")

  self.alertMessage = ko.observable("")
  self.alertVisible = ko.observable(false)

  self.clearCurrentPeriods = function(){
    self.currentPeriods.removeAll()
  }

  self.addPeriod = function(period){
    self.currentPeriods.push(period)
  }

  self.clearCurrentPeople = function(){
    self.currentPeople.removeAll()
  }

  self.addPerson = function(person){
    self.currentPeople.push(person)
  }

  // modal state and data
  self.modalDataType = ko.observable("")
  self.modalTitle = ko.observable("")
  self.modalSubtitle = ko.observable("")
}

var viewModel = new ViewModel()
var currentPeriodsSub = viewModel.currentPeriods.subscribe(function(periods){
  if(periods.length == viewModel.currentPeriodsLength() && periods.length > 0){
    viewModel.currentSelectedPeriodIndex(0)
    viewModel.currentSelectedFolderName(periods[0].folderName)
    setTimeout(function(){bindPeriod()}, 200)
    setTimeout(function(){
      $($('.period-item')[0]).parent().addClass('scrollable-item-selected')
      $(document.querySelectorAll('.period-item')[0].querySelector('div')).addClass('scrollable-item-text-selected')
    }, 200)
  }
})

var currentIsIDSub = viewModel.currentIsID.subscribe(function(isID){
  if(viewModel.currentIsID()){
    viewModel.currentLocale('id_ID')
  }else{
    viewModel.currentLocale('en_US')
  }
})

var currentSelectedFolderNameSub = viewModel.currentSelectedFolderName.subscribe(function(folderName){
  if(folderName != ""){
    read(viewModel.currentDataType(), viewModel.currentSelectedFolderName(), function(data){
      viewModel.currentPeople(data.people)
      viewModel.currentPositionsID(data.meta.id_ID.positions)
      viewModel.currentPositionsEN(data.meta.en_US.positions)
      setTimeout(function(){bindEditable()}, 200)
      //setTimeout(function(){dragInit()}, 200)
    })
  }
})

var currentPositionsENSub = viewModel.currentPositionsEN.subscribe(function(newArr){
  if(newArr.length > 0){
    var posEn = viewModel.currentPositionsEN()
    var posId = viewModel.currentPositionsID()
    viewModel.currentPositionsStrEN(posEn.join(","))
    viewModel.currentPositionsStrID(posId.join(","))
  }
})

var currentSelectedPersonEditModeSub = viewModel.currentSelectedPersonEditMode.subscribe(function(val){
  if(!val) viewModel.currentSelectedPersonImage(viewModel.currentSelectedPerson().image)  
})

ko.applyBindings(viewModel);

function reload(){

    periods(viewModel.currentDataType(), function(data){
      viewModel.clearCurrentPeriods()

      var periods = data.meta.en_US.years
      var folderNames = data.folderNames

      var periodsID = data.meta.id_ID.years

      viewModel.currentPeriodsLength(periods.length)
      viewModel.currentMetaData(data.meta)

      for(var i = 0; i < periods.length; i++){
        periods[i] = periods[i].split(' ').join('<br />')

        var period = { 
          'index' : i, 
          'period' : periods[i], 
          'periodObject' : { en_US : periods[i], id_ID : periodsID[i]},
          'folderName' : folderNames[i], 
          'lastFolder' : false
        }

        viewModel.addPeriod(period)

      }

      //viewModel.currentSelectedTitleEN(viewModel.currentMetaData()['en_US'].title)

      viewModel.currentSelectedTitleEN(viewModel.currentMetaData()['en_US'].title)
      viewModel.currentSelectedMenuTitleEN(viewModel.currentMetaData()['en_US'].menuTitle)
      viewModel.currentSelectedTitleID(viewModel.currentMetaData()['id_ID'].title)
      viewModel.currentSelectedMenuTitleID(viewModel.currentMetaData()['id_ID'].menuTitle)

      if(viewModel.currentDataType() != 'Founder')
      viewModel.addPeriod({ 'index' : periods.length, 'period' : 'Add', 'folderName' : 'add', 'lastFolder' : true})
    })
}

// control events
$('.board').click(function(){
  if($(this).attr('data-type') != viewModel.currentDataType()){
    $('.board').parent().removeClass('active')
    $(this).parent().addClass('active')
    viewModel.currentDataType($(this).attr('data-type'))
    reload()
  }
})

$('.lang').click(function(){
  $('.lang').addClass('secondary')
  $(this).removeClass('secondary')

  if($(this).text() == "EN"){
    viewModel.currentIsID(false)
  }else{
    viewModel.currentIsID(true)
  }
})

function showInputModal(el, type, prefix){

  closeAlertInputModal()

  if(!type)
    viewModel.modalDataType($(el).attr('data-type'))
  else
    viewModel.modalDataType(type)

  if(!prefix) prefix = ''
  viewModel.modalTitle(prefix + $(el).attr('data-main-title'))
  viewModel.modalSubtitle($(el).attr('data-subtitle'))

  var dataType = $(el).data('type')

  console.log(viewModel.currentPeople())

  if(dataType == 'person'){
    viewModel.currentSelectedPersonEditMode(false)
    var index = parseInt($(el).attr('data-index'))
    viewModel.currentSelectedPersonIndex(index)
    viewModel.currentSelectedPerson(viewModel.currentPeople()[index])
    viewModel.currentSelectedPersonImage(viewModel.currentSelectedPerson().image)

  }else if(dataType == 'menu-title'){
    $('#menu-title-en').val(viewModel.currentSelectedMenuTitleEN())
    $('#menu-title-id').val(viewModel.currentSelectedMenuTitleID())

  }else if(dataType == 'main-title'){
    $('#title-en').val(viewModel.currentSelectedTitleEN())
    $('#title-id').val(viewModel.currentSelectedTitleID())
  }

  $('#inputModal').foundation('reveal', 'open');
}

function closeInputModal(){
  $('#inputModal').foundation('reveal', 'close');
}

function bindEditable(){
  $('.editable').unbind()
  $('.editable').click(function(){
    showInputModal(this)
  })

  dragInit();
}

function bindPeriod(){
  $('.period-item').unbind()
  $('.period-item').click(function(){

    var self = this

    var folderName = $(self).attr('folder-name')

    if(folderName == 'add'){
      viewModel.currentSelectedPeriodIndex(-1)
      viewModel.currentSelectedPeriod({})
      showInputModal(self, 'period-add', "Add ")
    }
    else{
      $('.period-item').parent().removeClass('scrollable-item-selected')
      var periodItems = document.querySelectorAll('.period-item')

      for(var i = 0; i < periodItems.length; i++){
        var a = periodItems[i]
        var p = periodItems[i].querySelector('div')
        $(p).removeClass('scrollable-item-text-selected')
        if(folderName == $(a).attr('folder-name')){ 
          $(p).addClass('scrollable-item-text-selected')
        }
      }

      $(this).parent().addClass('scrollable-item-selected')

      if(folderName == viewModel.currentSelectedFolderName()){
        viewModel.currentSelectedPeriodIndex($(self).attr('data-index'))
        viewModel.currentSelectedPeriod(viewModel.currentPeriods()[viewModel.currentSelectedPeriodIndex()])
        showInputModal(self)
      }else{
        viewModel.currentSelectedFolderName(folderName)
        console.log('select new folder', folderName)
      }
    }
  })
}

function locale(){
  $('.lang').addClass('secondary')
  var arr = $('.lang');
  for(var i = 0; i < arr.length; i++){
    if(viewModel.currentIsID() && $(arr[i]).text() == "ID"){
      $(arr[i]).removeClass('secondary')
    }else if(!viewModel.currentIsID() && $(arr[i]).text() == "EN"){
      $(arr[i]).removeClass('secondary')
    }
  }
}

// specific binding

// person
$('#person-edit-save').click(function(){
  if(!viewModel.currentSelectedPersonEditMode()){
    viewModel.currentSelectedPersonEditMode(true)
    locale()
  }else{
    // save lang
    var data = {}
    var currentImg = viewModel.currentSelectedPersonImage()
    if(currentImg.indexOf('Temp') > -1){
      // image change
      // set name as the EN name
      data.currentImagePath = currentImg
      data.previousFilePath = viewModel.currentSelectedPerson().image
    }

    data.lang = viewModel.currentIsID() ? 'id_ID' : 'en_US'
    data.title = $('#person-name').val()
    data.subtitle = $('#person-subtitle').val()
    data.text = $('#person-text').val()

    $.post('/editPerson/' + viewModel.currentDataType() + '/' + viewModel.currentSelectedFolderName(), data, function(){
      viewModel.currentSelectedPersonEditMode(false)
      showAlertInputModal("Saved")
      locale()
    })

    
  }
})

$('#person-delete-cancel').click(function(){
  if(viewModel.currentSelectedPersonEditMode()){
    viewModel.currentSelectedPersonEditMode(false)
    locale()
  }else{
    console.log('delete person')
  }
})

$('#add-person').click(function(){
  viewModel.currentIsID(false)
  locale()
  showInputModal(this, 'person-add', "Add ")
  viewModel.currentSelectedPersonImage('Assets/img/default.jpg')
  $('#person-name-add').val('')
  $('#person-subtitle-add').val('')
  $('#person-text-add').val('')
})

$('#person-add-save').click(function(){
  var msg = ''
  if(viewModel.currentSelectedPersonImage() == 'Assets/img/default.jpg'){
    msg += 'image is mandatory'
  }

  if($('#person-name-add').val() == ''){
    if(msg.length > 0 ){ msg += ', '}
    msg += 'title is mandatory' 
  }

  if(msg.length > 0)
  showAlertInputModal(msg, true)
  else{
    var data = {
      lang : 'en_US',
      image : viewModel.currentSelectedPersonImage(),
      title : $('#person-name-add').val(),
      subtitle : $('#person-subtitle-add').val(),
      text : $('#person-text-add').val()
    }

    $.post('/addPerson/' + viewModel.currentDataType() + '/' + viewModel.currentSelectedFolderName(), data, function(res){
      console.log(res)
    })
  }
})


// periods
$('#button-period-add').click(function(){
  var dateId = $('#period-id').val()
  var dateEn = $('#period-en').val()

  if(dateId != 'INVALID' && dateId != '' && dateEn != ''){
    console.log(dateId)
    var folderName = dateId

    if(dateId.indexOf(' ') > -1){
      var arr = dateId.split(' ')
      console.log(dateId)
      var d = moment(dateEn, 'MMMM YYYY')
      var ma = d.month() + 1
      var m = ma < 10 ? ('0' + ma) : ma
      folderName = arr[1] + ' ' + m + '-' + arr[0]
    }

    var value = {
      ID : $('#period-id').val(), 
      EN : $('#period-en').val(),
      folderName : folderName
    }
    
    $.post('/addPeriod/' + viewModel.currentDataType(), { value : value }, function(data){
      if(data.error){
        showAlertInputModal(data.message, true)
      }
      else{
        closeInputModal()
      }
      //
    })
  }else{
    showAlertInputModal('Invalid period', true)
  }

})

$('#button-period-delete').click(function(){
  console.log('remove period')
})

$('#button-title').click(function(){
  var type = 'main-title'
  var data = { type : type, value : {ID : $('#title-id').val(), EN : $('#title-en').val()}}
  $.post('/editTitle/' + viewModel.currentDataType(), data, function(){
    reload()
    closeInputModal()
  })
})

$('#button-menu-title').click(function(){
  var type = 'menu-title'
  var data = { type : type, value : {ID : $('#menu-title-id').val(), EN : $('#menu-title-en').val()}}
  $.post('/editTitle/' + viewModel.currentDataType(), data, function(){
    reload()
    closeInputModal()
  })
})

function showAlertInputModal(msg, error){
  viewModel.alertVisible(true)
  $('#alert-input').removeClass('alert')
  if(error) {
    $('#alert-input').addClass('alert')
  }
  viewModel.alertMessage(msg)
  setTimeout(function(){viewModel.alertVisible(false)}, 600)

}

function closeAlertInputModal(){
  viewModel.alertVisible(false)
  viewModel.alertMessage("")
  
}

/*$('.close').click(function(){
  closeAlertInputModal()
})


$('#period-en').keyup(function(){
  closeAlertInputModal()

  var a = $(this).val().trim()
  if(a.indexOf(' ') > -1){
    if(moment(a, 'MMMM YYYY').isValid()){
      var b = moment(a, 'MMMM YYYY')
      b.lang('id')
      $('#period-id').val(b.format('MMMM YYYY'))
    }else{
      $('#period-id').val('INVALID')
    }
  }else{

    if(moment(a, 'YYYY').isValid()){
      var b = moment(a, 'YYYY')
      b.lang('id')
      $('#period-id').val(b.format('YYYY'))
    }else{
      $('#period-id').val('INVALID')
    }

  }
})

$('#person-image').click(function(){
  $('#fileupload').trigger('click'); 
})

$('#person-image-add').click(function(){
  $('#fileupload').trigger('click'); 
})

// page
function init(){
  viewModel.currentIsID(false)
  $('#board-founder').click()
}

// server
function periods(dataType, callback){
  $.get('/list/' + dataType, callback)
}

function read(dataType, folderName, callback){
  $.get('/read/' + dataType + '/' + folderName, callback)
}

function handlePeopleSequence(){

  var currentPeopleIndexes = []
  
  $('.person').each(function(){
    currentPeopleIndexes.push($(this).data('index'))
  })

  var currentPeople = []

  for(var i = 0; i < currentPeopleIndexes.length; i++){
    var arr = viewModel.currentPeople()
    var idx = currentPeopleIndexes[i]
    currentPeople.push(viewModel.currentPeople()[idx])
    
    var prefixIdx = currentPeople[i].image.lastIndexOf('/')
    var prefix = currentPeople[i].image.substr(0, prefixIdx)
    var suffix = currentPeople[i].image.substr(prefixIdx + 1, currentPeople[i].image.length)
    
    if(suffix.indexOf('_') > -1){
      suffix = suffix.substr(suffix.indexOf('_') + 1, suffix.length)
    }

    currentPeople[i].image = prefix + '/' + i + '_' + suffix
    currentPeople[i].index = i
  }

  console.log(currentPeople)
}

$(function () {
    $('#fileupload').fileupload({
        dataType: 'json',
        done: function (e, data) {
           viewModel.currentSelectedPersonImage(data.result.tempPath)
        }
    });
});

// main
//init()*/




